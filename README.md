# Customer API #

This repo contains a RAML API definition and a MuleSoft project to build a customer API

## Requirements

The requirement for this project were to develop a Customer API that clients could:

* List customers
* Create a new customer
* Update a customer
* Deletes a customer

The following use cases were also provided:

1. A consumer may periodically (every 5 minutes) consume the API to enable it (the consumer) to maintain a copy of the provider API's customers (the API represents the system of record)
2. A mobile application used by customer service representatives that uses the API to retrieve and update the customers details
3. Simple extension of the API to support future resources such as orders and products

As an optional stretch goal, please implement the RESTful API in Mulesoft with test stubs for the consumer and provider systems.

## Assumptions

A number of assumption were made with respect to the requirements.

### Provider Behaviour
Simplifying assumptions have been made with respect to the provider platform. They include:

  * The provider is performant and reliable. All interactions (CREATE, DELETE, UPDATE, GET) can be performed synchronously.
  * The number of customer records are not significant e.g. 10,000 - 20,000 records.  

### Resources

I have made the following assumptions in the modelling of the Customer and Address types:

  * A Customer must have a first name, last name and at least one Address.
  * Address is not treated as a separate resource.
  * For the sake of brevity in this hypothetical example, enumerated values of address attributes are incomplete.

##Use Case Assessment

### Assessment of use case 1 - polling consumer
This use case highlights two primary issues to be addressed:

  1. Polling behaviour - The inclusion of the statement that the consumer may poll every 5 minutes implies that form of restriction may need to be applied.
  2. They will extract the entire Customer set which may contain many thousands of records (see simplifying assumptions).

### Implementation
In my implementation the polling consumer will use the customers resource to extract the list of consumers. To minimise the impact of retrieving a large dataset I have included the following facilities:

  * Pagination - The results are returned in pages by default to prevent significant amounts of data being returned in one call. The maximum page size is configurable within the RAML API so this can be tuned as required.
  * Rate Limiting - Clients polling for pages can be limited through rate limiting. Custom headers are used to indicate to client their usage. These are configurable in the RAML so they can be tuned.
  * Compression - I have included the use of the Accept-Encoding and Content-Encoding headers in the request and response respectively to compress the dataset before sending it over the network. This should allow for larger pages to be transmitted at a time.

#### Alternatives
An alternate solution would be to provide an asynchronous download facility. I would implement this as follows:

  1. Create a new resource called CustomerFile
  2. Client submits a POST request to create a new CustomerFile
  3. API responds with a 202 Accepted and includes a URL to another resource Jobs in the Location header which represents Jobs that the API is processing.
  4. The client can query the queue using a GET request to get the status of the Jobs to get updates on the job.
  5. When the job completes a GET request to the Jobs resource will return a 303 redirect back to the CustomerFile resource with the URL in the Location header.
  6. The client then downloads the resource as a file.

I chose not to implement this approach as its a more complex solution and may be unwarranted. If the assumption of the dataset holds true then a simple paging strategy could support both the mobile and polling consumer scenarios. This simplifies development and operations.

### Assessment of use case 2 - mobile consumer
I have assumed that the following interactions occur for the mobile user:

  1. User requests a list of Customers.
  2. User selects a User.
  3. User updates 1..n Customer fields.
  4. User submits update.

The primary concern with the mobile user is effective use of a slow unreliable network. This indicates that only the minimum set of data should be transferred over the network. To address this concern I have included:

  * Sparse field sets support - When retrieving the list of customers the mobile client can restrict the data displayed to only those fields that will be displayed e.g. firstName, lastName, address.formatted.
  * Compression - The mobile can request all responses be compressed limiting the amount of data transferred over the network. It would need to be assessed as to whether the compress/decompress time is less that the time to transmit the delta of the compressed and uncompressed payloads.
  * PATCH support - The mobile client only has to upload the fields that they wish to change. This limits the amount of data transmitted. This operation supports conditional updates. See other implementation concerns for further details.
  * Conditional Get - The GET request supports the If-Modified-Since and If-None-Match headers. If the client requests a resource and it has not changed on the server it will respond with a HTTP status of 304 with no body, further minimising data transfer.
  * Paging - same as use case 1.

### Assessment of use case 3 - extensibility
The RAML documentation has been generalised and parameterised in a number of ways:

  * ResourceTypes - The behaviour of the Customer API is specified using the collection and collectionItem resourceTypes. These resourceTypes have been parameterised and do not refer in any way to the Customer resource. They have also been put into a separate file. This allows these definitions to be reused in the future by the Products and Orders API's without change by just importing them into their RAML definitions.
  * Traits - I have extracted common functions into traits where possible. This includes the ability to support:
    * compression
    * sparse field sets
    * paging
    * rate limiting

    Even if the Products or Orders API's need a different implementation to that defined in the resourceTypes these traits can still be re-used so that the behaviour of these functions is consistent for all API's.

    Traits were parameterised where required so that different API's can set different values where needed (see the rateLimitable trait for an example of this where the maximum page size is configurable).
  * Common Types - I created a library of common types that was used for types that are not API specific. At present this only contains the Error type but could include other types in the future e.g. if JSON-API were to be implemented. Similar to above, the common types provide a consistent representation of error messages for all consumers.
  * Examples - I have extracted all the examples into a common folder to improve readability of the document.

### Other implementation concerns
There are few other things that I thought through in the development of the API.

  * Usage of JSON-API - I considered using JSON-API for representing resources. I've used it in the past and found its standardisation useful. I did try but I wasn't able to find a clean way to separate the envelope from the attributes and relationships using RAML. I've used OpenAPI in the past and had a similar problem. To keep it simple I just decided to leave it out.
  * Versioning URL vs. Content Negotiation - I chose to use the URL in the versioning of the API only because its the simpler solution. I think there are trade offs in either approach.
  * GET returns an array - I chose to have the GET return an array. In my opinion this simplifies client consumption. A client only has to build one implementation to interact with the Customer resource i.e. It's always extracting Customer resources from an array even if only one is returned.
  * Conditional Updates - In addition to the PATCH operation the DELETE and PATCH operation support conditional updates. If a client attempts to update a resource that has already been updated by another user then the API will return a HTTP 412 indicating that the resource was previously modified. To enable this, on these HTTP methods the client must provide the If-Unmodified-Since and If-Match headers. To populate these headers the API will return the ETag and Last-Modified headers in the response to a GET for a specific Customer resource e.g. customers/1.
  * Multiple Customer types - There are 3 types that I defined in the RAML spec:

    * updateableCustomer - What a client can update
    * newCustomer - What a client must supply to create
    * customer - What a client can retrieve.

    This has a couple of benefits over a single customer specification:

    * It makes it easy for clients to understand what they can and can't modify and what is mandatory and optional.
    * When creating samples the editor is able to detect errors which improves the quality of the samples.
    * When developers make changes it forces them to think about these characteristics of the resource which they may not have done if just adding it to a generic object.

    I found point 2 particularly useful in just this small exercise.

## MuleSoft Implementation
In my implementation I assume that the API is a simple proxy over an HTTP service that supports XML payloads.

As I have no prior experience with MuleSoft I was only able to complete a small portion of the implementation within the time constraints. The few things that I managed to do were:

* Import the RAML definition I had crafted previously to generate the project shell. I thought this was a good idea as it:

    * Linked the documentation directly to the implementation.
    * Enabled the auto generation of the main flows, reducing development time.
    * Generates validations for consumer requests automatically based on the RAML file.

* Build the sunny day scenario of the /customers/{id} flow by generating an artificial provider XML schema and using a data transformer component to map the XML to JSON output.
* Generated a set of test cases based from the APIKit router to drive a TDD based development approach. Most of these are still generating errors that need to be fixed.
* Generate a mock that could simulate a provider response for the above scenario.
* Simplify the verification of payloads by using a custom assertion that utilised the JSONAssert library over exactly like for like payload comparisons. It's a bit clunky but it works.

So overall...not much in the way of development. I tried several approaches to build out a more complete example including error handling for the non successful responses from the provider but ran into issues either getting the flows to route correctly or the APIKit global exception mappers handling the error response successfully.
