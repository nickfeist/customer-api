package com.acme.api.exceptions;

public class ApiHttp400PageUnderflowException extends ApiException {

	private static final String CODE = "ERR-0004";
	private static final String TITLE = "Pagination Error";
	private static final String DESCRIPTION = "The value of start cannot be less than 0.";
	
	@Override
	protected ErrorCode getErrorCode() {		
		return new ErrorCode(CODE, TITLE, DESCRIPTION);
	}
}
