package com.acme.api.exceptions;

public class ApiHttp400MaxPageSizeExceededException extends ApiException {

	private static final String CODE = "ERR-0008";
	private static final String TITLE = "Pagination Error";
	private static final String DESCRIPTION = "The page size cannot exceed 1000 records.";
	
	@Override
	protected ErrorCode getErrorCode() {		
		return new ErrorCode(CODE, TITLE, DESCRIPTION);
	}
}
