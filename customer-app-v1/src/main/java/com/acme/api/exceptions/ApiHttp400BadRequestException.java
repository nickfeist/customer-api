package com.acme.api.exceptions;

public class ApiHttp400BadRequestException extends ApiException {

	private static final String CODE = "ERR-0002";
	private static final String TITLE = "General Error";
	private static final String DESCRIPTION = "Bad request.";
	
	@Override
	protected ErrorCode getErrorCode() {
		return new ErrorCode(CODE, TITLE, DESCRIPTION);
	}

}
