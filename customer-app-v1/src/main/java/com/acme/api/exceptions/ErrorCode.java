package com.acme.api.exceptions;

/**
 * Domain object that represents an API error
 * 
 * @author nick
 *
 */
public class ErrorCode {

	private final String code;	
	private final String title;
	private final String description;
	
	public ErrorCode(String code, String title, String description) {
		this.code = code;
		this.title = title;
		this.description = description;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getDescription() {
		return description;
	}
}
