package com.acme.api.exceptions;

/**
 * An exception for a generic 500 server response.
 * 
 * @author nick
 *
 */

public class ApiHttp500Exception extends ApiException{
	
	private static final long serialVersionUID = 6930749258527805208L;
	
	private static final String CODE = "ERR-0001";
	private static final String TITLE = "General Error";
	private static final String DESCRIPTION = "An unknown error has occured while processing your request.";
	
	public ApiHttp500Exception() {
		super();
	}

	@Override
	protected ErrorCode getErrorCode() {
		return new ErrorCode(CODE, TITLE, DESCRIPTION);
	}

}
