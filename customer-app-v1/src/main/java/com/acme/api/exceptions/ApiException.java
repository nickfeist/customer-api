package com.acme.api.exceptions;

/**
 * This exception class represents an API Exception.
 * 
 * It models the content of a non 2xx error response that API's can return.
 * 
 * This class provides the base logic for API exceptions.
 * Sub classes are expected to define the ErrorCode object.
 * 
 * @author nick
 *
 */
public abstract class ApiException extends Exception {
	
	private static final String MESSAGE_STRING = "{\"code\": \"%s\",\"title\": \"%s\",\"detail\": \"%s\"}";
	
	private final ErrorCode error;	
	private final String message;
	
	public ApiException() {		
		error = getErrorCode();
		this.message = String.format(MESSAGE_STRING, error.getCode(), error.getTitle(), error.getDescription());
	} 
	
	/**
	 * Gets the error code.
	 * 
	 *  
	 * @return ErrorCode
	 */
	protected abstract ErrorCode getErrorCode();
	
	
	@Override
	public String getMessage() {
		return this.message;
	}
	
	public ErrorCode getError() {
		return error;
	}
}
