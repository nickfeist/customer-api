/**
 * 
 */
package com.acme.api.transformers;

import java.util.Map;

import org.mule.api.MuleContext;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.el.context.MessageContext;
import org.mule.transformer.AbstractMessageTransformer;

import com.acme.api.exceptions.ApiException;
import com.acme.api.exceptions.ApiHttp400BadRequestException;
import com.acme.api.exceptions.ApiHttp400MaxPageSizeExceededException;
import com.acme.api.exceptions.ApiHttp400PageUnderflowException;

/**
 * @author nick
 *
 */
public class PaginationTransformer {

	private static final int DEFAULT_START = 0;
	private static final int DEFAULT_LIMIT = 10;
	private static final int DEFAULT_MAX_PAGE = 100;
	
	private static final String FLOW_VAR_NAME_START = "start";
	private static final String FLOW_VAR_NAME_LIMIT = "limit";

	private int defaultStart = DEFAULT_START;
	private int defaultLimit = DEFAULT_LIMIT;
	private int maximumPageSize = DEFAULT_MAX_PAGE;

	public MuleMessage validatePagination(MuleMessage msg) throws ApiException {		
		int start;
		int limit;
		
		Map<String, String> queryParams = msg.getInboundProperty("http.query.params");
		start = this.calaculatePageStart(queryParams.get("start"));
		limit = this.calculateLimitStart(queryParams.get("limit"));

		if (start < 0) {
			throw new ApiHttp400PageUnderflowException();
		}

		if (limit > this.maximumPageSize) {
			throw new ApiHttp400MaxPageSizeExceededException();
		}

		msg.setOutboundProperty(FLOW_VAR_NAME_START, Integer.toString(start));
		msg.setOutboundProperty(FLOW_VAR_NAME_LIMIT, Integer.toString(limit));
		System.out.println("THIS EXECUTED");
		return msg;
	}
	
	private int calaculatePageStart(String startParam) throws ApiHttp400BadRequestException {
		int start;
		if (startParam == null) {
			start = this.defaultStart;
		} else {
			try {
				start = Integer.parseInt(startParam);
			} catch (NumberFormatException ex) {
				throw new ApiHttp400BadRequestException();
			}
		}
		return start;
	}
	
	private int calculateLimitStart(String limitParam) throws ApiHttp400BadRequestException {
		int limit;
		
		if (limitParam == null) {
			limit = this.defaultLimit;
		} else {
			try {							
				limit = Integer.parseInt(limitParam);
			} catch (NumberFormatException ex) {
				throw new ApiHttp400BadRequestException();
			}
		}		
		return limit;
	}
}
