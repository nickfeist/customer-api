/**
 * 
 */
package com.acme.api.exceptions;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author nick
 *
 */
public class ApiExceptionTests {

	@Test
	public void testMessageFormatsCorrectly() {
		
		final String expectedResponse = "{\"code\": \"ERR-0001\",\"title\": \"General Error\",\"detail\": \"An unknown error has occured while processing your request.\"}";
		
		ApiException e = new ApiHttp500Exception();		
		
		assertEquals(expectedResponse, e.getMessage());				
	}

}
