package com.nickfeist.custom.assertions;

import org.mule.api.MuleEvent;
import org.mule.munit.assertion.MunitAssertion;
import org.skyscreamer.jsonassert.JSONAssert;

/**
 * This is a basic wrapper class around the JSONAssert library so that it can be used in MUnit testing.
 * 
 * Assertions are performed in a lenient manner. Ordering and additional attributes are ignored.
 * 
 * This assertion has a dependency on the MuleEvent containing the payload for comparison.
 * It should be included as a variable prior to the assertion being called.
 * There is probably a better way but this is the best I could come up with given time constraints.
 * 
 * @author nick
 *
 */
public class JsonAsserter implements MunitAssertion {
	
	private static final String EXPECTED_PAYLOAD_KEY = "expected";
	@Override
	  public MuleEvent execute(MuleEvent muleEvent) throws AssertionError {
		
		String actual = muleEvent.getMessage().getPayload().toString();
		String expected = muleEvent.getMessage().getInvocationProperty(EXPECTED_PAYLOAD_KEY, "");		
	    JSONAssert.assertEquals(expected, actual, false);

	    return muleEvent;                                                       

	  }
}